package com.shaksham.interfaces;

public interface TextCallBackListener {
    public void updateText(String val);
}
