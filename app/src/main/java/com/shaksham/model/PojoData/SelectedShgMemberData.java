package com.shaksham.model.PojoData;

public class SelectedShgMemberData {
    private String selectedPosition;
    private String selectedMemberCode;
    private String selectedMemberName;

    public String getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(String selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public String getSelectedMemberCode() {
        return selectedMemberCode;
    }

    public void setSelectedMemberCode(String selectedMemberCode) {
        this.selectedMemberCode = selectedMemberCode;
    }

    public String getSelectedMemberName() {
        return selectedMemberName;
    }

    public void setSelectedMemberName(String selectedMemberName) {
        this.selectedMemberName = selectedMemberName;
    }
}
