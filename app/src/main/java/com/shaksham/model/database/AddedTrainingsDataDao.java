package com.shaksham.model.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "ADDED_TRAININGS_DATA".
*/
public class AddedTrainingsDataDao extends AbstractDao<AddedTrainingsData, Long> {

    public static final String TABLENAME = "ADDED_TRAININGS_DATA";

    /**
     * Properties of entity AddedTrainingsData.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property TrainingId = new Property(1, String.class, "trainingId", false, "TRAINING_ID");
        public final static Property Blockcode = new Property(2, String.class, "blockcode", false, "BLOCKCODE");
        public final static Property GpCode = new Property(3, String.class, "gpCode", false, "GP_CODE");
        public final static Property VillageCode = new Property(4, String.class, "villageCode", false, "VILLAGE_CODE");
        public final static Property TrainingEvaluationDate = new Property(5, String.class, "trainingEvaluationDate", false, "TRAINING_EVALUATION_DATE");
        public final static Property TrainingAddedDateDate = new Property(6, String.class, "trainingAddedDateDate", false, "TRAINING_ADDED_DATE_DATE");
        public final static Property TrainingSyncDateTime = new Property(7, String.class, "trainingSyncDateTime", false, "TRAINING_SYNC_DATE_TIME");
        public final static Property EvaluationStatusForTraining = new Property(8, String.class, "evaluationStatusForTraining", false, "EVALUATION_STATUS_FOR_TRAINING");
        public final static Property SubTotalShgMember = new Property(9, String.class, "subTotalShgMember", false, "SUB_TOTAL_SHG_MEMBER");
        public final static Property OtherParticipant = new Property(10, String.class, "otherParticipant", false, "OTHER_PARTICIPANT");
        public final static Property TotalParticipant = new Property(11, String.class, "totalParticipant", false, "TOTAL_PARTICIPANT");
        public final static Property EvaluationMaximumDate = new Property(12, String.class, "evaluationMaximumDate", false, "EVALUATION_MAXIMUM_DATE");
    }


    public AddedTrainingsDataDao(DaoConfig config) {
        super(config);
    }
    
    public AddedTrainingsDataDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"ADDED_TRAININGS_DATA\" (" + //
                "\"_id\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "\"TRAINING_ID\" TEXT," + // 1: trainingId
                "\"BLOCKCODE\" TEXT," + // 2: blockcode
                "\"GP_CODE\" TEXT," + // 3: gpCode
                "\"VILLAGE_CODE\" TEXT," + // 4: villageCode
                "\"TRAINING_EVALUATION_DATE\" TEXT," + // 5: trainingEvaluationDate
                "\"TRAINING_ADDED_DATE_DATE\" TEXT," + // 6: trainingAddedDateDate
                "\"TRAINING_SYNC_DATE_TIME\" TEXT," + // 7: trainingSyncDateTime
                "\"EVALUATION_STATUS_FOR_TRAINING\" TEXT," + // 8: evaluationStatusForTraining
                "\"SUB_TOTAL_SHG_MEMBER\" TEXT," + // 9: subTotalShgMember
                "\"OTHER_PARTICIPANT\" TEXT," + // 10: otherParticipant
                "\"TOTAL_PARTICIPANT\" TEXT," + // 11: totalParticipant
                "\"EVALUATION_MAXIMUM_DATE\" TEXT);"); // 12: evaluationMaximumDate
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"ADDED_TRAININGS_DATA\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, AddedTrainingsData entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String trainingId = entity.getTrainingId();
        if (trainingId != null) {
            stmt.bindString(2, trainingId);
        }
 
        String blockcode = entity.getBlockcode();
        if (blockcode != null) {
            stmt.bindString(3, blockcode);
        }
 
        String gpCode = entity.getGpCode();
        if (gpCode != null) {
            stmt.bindString(4, gpCode);
        }
 
        String villageCode = entity.getVillageCode();
        if (villageCode != null) {
            stmt.bindString(5, villageCode);
        }
 
        String trainingEvaluationDate = entity.getTrainingEvaluationDate();
        if (trainingEvaluationDate != null) {
            stmt.bindString(6, trainingEvaluationDate);
        }
 
        String trainingAddedDateDate = entity.getTrainingAddedDateDate();
        if (trainingAddedDateDate != null) {
            stmt.bindString(7, trainingAddedDateDate);
        }
 
        String trainingSyncDateTime = entity.getTrainingSyncDateTime();
        if (trainingSyncDateTime != null) {
            stmt.bindString(8, trainingSyncDateTime);
        }
 
        String evaluationStatusForTraining = entity.getEvaluationStatusForTraining();
        if (evaluationStatusForTraining != null) {
            stmt.bindString(9, evaluationStatusForTraining);
        }
 
        String subTotalShgMember = entity.getSubTotalShgMember();
        if (subTotalShgMember != null) {
            stmt.bindString(10, subTotalShgMember);
        }
 
        String otherParticipant = entity.getOtherParticipant();
        if (otherParticipant != null) {
            stmt.bindString(11, otherParticipant);
        }
 
        String totalParticipant = entity.getTotalParticipant();
        if (totalParticipant != null) {
            stmt.bindString(12, totalParticipant);
        }
 
        String evaluationMaximumDate = entity.getEvaluationMaximumDate();
        if (evaluationMaximumDate != null) {
            stmt.bindString(13, evaluationMaximumDate);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, AddedTrainingsData entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String trainingId = entity.getTrainingId();
        if (trainingId != null) {
            stmt.bindString(2, trainingId);
        }
 
        String blockcode = entity.getBlockcode();
        if (blockcode != null) {
            stmt.bindString(3, blockcode);
        }
 
        String gpCode = entity.getGpCode();
        if (gpCode != null) {
            stmt.bindString(4, gpCode);
        }
 
        String villageCode = entity.getVillageCode();
        if (villageCode != null) {
            stmt.bindString(5, villageCode);
        }
 
        String trainingEvaluationDate = entity.getTrainingEvaluationDate();
        if (trainingEvaluationDate != null) {
            stmt.bindString(6, trainingEvaluationDate);
        }
 
        String trainingAddedDateDate = entity.getTrainingAddedDateDate();
        if (trainingAddedDateDate != null) {
            stmt.bindString(7, trainingAddedDateDate);
        }
 
        String trainingSyncDateTime = entity.getTrainingSyncDateTime();
        if (trainingSyncDateTime != null) {
            stmt.bindString(8, trainingSyncDateTime);
        }
 
        String evaluationStatusForTraining = entity.getEvaluationStatusForTraining();
        if (evaluationStatusForTraining != null) {
            stmt.bindString(9, evaluationStatusForTraining);
        }
 
        String subTotalShgMember = entity.getSubTotalShgMember();
        if (subTotalShgMember != null) {
            stmt.bindString(10, subTotalShgMember);
        }
 
        String otherParticipant = entity.getOtherParticipant();
        if (otherParticipant != null) {
            stmt.bindString(11, otherParticipant);
        }
 
        String totalParticipant = entity.getTotalParticipant();
        if (totalParticipant != null) {
            stmt.bindString(12, totalParticipant);
        }
 
        String evaluationMaximumDate = entity.getEvaluationMaximumDate();
        if (evaluationMaximumDate != null) {
            stmt.bindString(13, evaluationMaximumDate);
        }
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public AddedTrainingsData readEntity(Cursor cursor, int offset) {
        AddedTrainingsData entity = new AddedTrainingsData( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // trainingId
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // blockcode
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // gpCode
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // villageCode
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // trainingEvaluationDate
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // trainingAddedDateDate
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // trainingSyncDateTime
            cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8), // evaluationStatusForTraining
            cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9), // subTotalShgMember
            cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10), // otherParticipant
            cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11), // totalParticipant
            cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12) // evaluationMaximumDate
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, AddedTrainingsData entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setTrainingId(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setBlockcode(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setGpCode(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setVillageCode(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setTrainingEvaluationDate(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setTrainingAddedDateDate(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setTrainingSyncDateTime(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setEvaluationStatusForTraining(cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8));
        entity.setSubTotalShgMember(cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9));
        entity.setOtherParticipant(cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10));
        entity.setTotalParticipant(cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11));
        entity.setEvaluationMaximumDate(cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(AddedTrainingsData entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(AddedTrainingsData entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(AddedTrainingsData entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
